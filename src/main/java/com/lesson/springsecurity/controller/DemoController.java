package com.lesson.springsecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DemoController {

	@GetMapping("/")
	public String homePage() {
		return "home";
	}
	
	@GetMapping("/leaders")
	public String forAdmin() {
		return "leaders";
	}
	@GetMapping("/systems")
	public String formanager() {
		return "systems";
	}
}

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
	<head><title>This is Title</title></head>
	
	
	<body>
		<h2>This is Home Page</h2>
		<p>
	This for Practice
	</p>
	
	<hr>
		<p>
			User: <security:authentication property="principal.username"/>
			<br><br>
			Role(s): <security:authentication property="principal.authorities"/>
		</p>
	<hr>
	<security:authorize access="hasRole('Admin')">
		<p>
		<a href="${pageContext.request.contextPath}/leaders">LeaderShip Meeting</a>
		(Only For Admin)
		</p>
	</security:authorize>
		
		<security:authorize access="hasRole('Manager')">
			<p>
		<a href="${pageContext.request.contextPath}/systems">It Systems Meeting</a>
		(Only For Manager)
		</p>
		
		</security:authorize>
	<hr>
	<form:form action="${pageContext.request.contextPath}/logout" method="POST">
		<input type="submit" value="Logout" />
	</form:form>
	</body>
</html>